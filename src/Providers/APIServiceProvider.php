<?php

namespace Mediapress\API\Providers;

use Illuminate\Support\ServiceProvider;


class APIServiceProvider extends ServiceProvider
{


    public function boot()
    {

        $this->loadViewsFrom(__DIR__ . '/..' . DIRECTORY_SEPARATOR . "Resources" . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'panel', 'mediapressApi');
        $this->publishes([__DIR__ . '/../Assets' => public_path('vendor/api')], 'ApiAssets');
        $this->publishes([__DIR__ . '/..' . DIRECTORY_SEPARATOR . 'Config' => config_path()], 'ApiPublishes');
    }

    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
