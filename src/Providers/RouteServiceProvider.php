<?php

namespace Mediapress\API\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{

    public $namespace = 'Mediapress\API\Http\Controllers';
    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapPanelRoutes();
        $this->mapWebRoutes();

    }

    protected function mapWebRoutes()
    {
        Route::middleware('api')
            ->namespace($this->namespace . '\Web')
            ->prefix( 'api/v4.1')
           ->group(__DIR__.'/../Routes/web.php');
    }

    /**
     * Define the "panel" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */

    protected function mapPanelRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Panel')
            ->prefix( 'mp-admin')
            ->group(__DIR__.'/../Routes/panel.php');
    }
}
