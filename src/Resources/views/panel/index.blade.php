<link rel="stylesheet" href="{!! asset('/vendor/api/css/swagger-ui.css') !!}"/>

<div id="swagger-ui"></div>

<script src="{!! asset("/vendor/api/js/swagger-ui-bundle.js") !!}"></script>
<script>
    const ui = SwaggerUIBundle({
        url: '{!! url("/mp-admin/api/json") !!}',
        dom_id: '#swagger-ui',
        presets: [
            SwaggerUIBundle.presets.apis,
            SwaggerUIBundle.SwaggerUIStandalonePreset
        ],
    })
</script>
