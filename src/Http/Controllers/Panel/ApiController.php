<?php

namespace Mediapress\API\Http\Controllers\Panel;



use Mediapress\Http\Controllers\PanelController as Controller;

class ApiController extends Controller
{
    public function index() {
        return view('mediapressApi::index');
    }
    public function json() {
        $file = json_decode(file_get_contents(base_path('/vendor/mediapress/api/src/Assets/api.json')), 1);
        $file['host'] = request()->server('HTTP_HOST');
        return response()->json($file);
    }
}
