<?php

namespace Mediapress\API\Http\Controllers\Web\Get;

use Mediapress\API\Http\Controllers\Web\ApiController;

use Carbon\Carbon;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Heraldist\Models\Form;
use Mediapress\Modules\Heraldist\Models\Message;

class FormController extends ApiController
{

    public function index($id = null)
    {
        $this->type = 'form';
        try {
            $this->status = true;
            $this->code = 200;
            $this->payload = $this->getPayload($id);
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();
    }

    function getPayload($id)
    {
        if (!$id) {
            return $this->getForms();
        }
        return $this->getFormFields($id);
    }

    private function getForms()
    {
        $forms = Form::all();
        if ($forms->count() <= 0) {
            return $this->emptyData();
        }

        $hold = array();
        foreach ($forms as $form) {
            $hold[] = [
                'id' => $form->id,
                'name' => $form->name,
                'receiver' => explode(',', str_replace(' ', '', $form->receiver)),
            ];
        }
        return $hold;
    }

    private function getFormFields($id)
    {
        $form = Form::findOrFail($id);
        $render = $form->render;

        $hold = [
            'tag' => 'form',
            'attr' => [
                'action' => $render->url,
                'method' => 'post'
            ],
            'children' => [
                [
                    'tag' => 'input',
                    'attr' => [
                        'type' => 'hidden',
                        'name' => '_id',
                        'ref' => '_id',
                        'value' => encrypt($form->id)
                    ]
                ]
            ]
                +
            $this->setFields($render->fields)
        ];

        return $hold;
    }


    private function setFields($form_fields) {

        $hold = array();
        foreach ($form_fields as $key => $field) {
            $key++;
            if(in_array($field['key'], ['rows', 'columns'])) {
                $hold[$key] = [
                    'key' => $field['key'],
                    'tag' => 'div',
                ];
            } else {
                $hold[$key]['tag'] = $field['tag'];
            }

            if(isset($field['id'])) {
                $hold[$key]['attr']['id'] = $field['id'];
                $hold[$key]['attr']['name'] = $hold[$key]['attr']['ref'] = hash('ripemd128', $field['id']);
            }

            if(isset($field['className'])) {
                $hold[$key]['attr']['className'] = $field['className'];
            }

            if(isset($field['attrs'])) {
                $hold[$key]['attr'] += $field['attrs'];
            }

            if(isset($field['config']['label'])) {
                $hold[$key]['label'] = langPart($field['config']['label'], $field['config']['label'], [], $this->language['id']);
            }

            if(isset($field['children'])) {
                $hold[$key]['children'] = $this->setFields($field['children']);
            }
        }
        return $hold;

    }
}


