<?php

namespace Mediapress\API\Http\Controllers\Web\Get;

use Mediapress\API\Http\Controllers\Web\ApiController;

use Carbon\Carbon;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Heraldist\Models\Form;
use Mediapress\Modules\Heraldist\Models\Message;
use Mediapress\Modules\Auth\Models\Admin;

class MessageController extends ApiController
{
    private $user_status = false;

    public function index($id = null)
    {
        $this->type = 'message';
        try {
            $this->status = true;
            $this->code = 200;
            $this->payload = $this->getPayload($id);
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();
    }

    function getPayload($id)
    {
        if (!$id) {
            throw new \Exception('id not found');
        }

        $api_token = request()->get('api_token');

        if ($api_token) {
            $user = Admin::where('api_token', $api_token)->first();
            if($user) {
                $this->user_status = true;
            }
        }

        if ($this->user_status === false) {
            throw new \Exception("User Bulunamadı !");
        }

        return $this->getMessages($id);
    }


    private function getMessages($id)
    {

        $messages = Message::where('form_id', $id);

        $hold['pagination_status'] = $this->pagination_status;


        if ($this->pagination_status) {
            $messages = $messages->paginate($this->per_page, ["*"], 'p');
            $new_path = str_replace(["&p=" . request()->get('p'), "p=" . request()->get('p')], "", request()->getRequestUri());
            $messages->withPath(url($new_path));
            $items = $messages->getCollection();
            $messages_arr = $messages->toArray();


            unset($messages_arr['data']);
            return [
                'paginate' => $messages_arr,
                'content' => $this->setMessages($items)
            ];

        } else {
            $messages = $messages->get();

            return [
                'paginate' => false,
                'content' => $this->setMessages($messages)
            ];
        }
    }

    private function setMessages($messages)
    {

        $hold = array();
        foreach ($messages as $message) {
            $temp = [];
            foreach ($message->data as $input) {
                if (isset($input['label']) && $input['label'] != "") {
                    $temp[$input['label']] = $input['value'];
                }
            }
            $hold['messages'][] = [
                'name' => $message->name,
                'date' => Carbon::parse($message->created_at)->format('d-m-Y, H:i:s'),
                'email' => $message->email,
                'ip' => $message->ip,
                'agent' => $message->agent,
                'data' => $temp,
            ];
        }
        return $hold;
    }

}


