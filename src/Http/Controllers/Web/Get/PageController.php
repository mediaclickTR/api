<?php

namespace Mediapress\API\Http\Controllers\Web\Get;

use Mediapress\API\Http\Controllers\Web\ApiController;

use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\MPCore\Models\Url;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\Criteria;

class PageController extends ApiController
{
    private $ids = array();

    public function index($id = null)
    {
        $this->type = "pages";
        try {
            $this->status = true;
            $this->code = 200;
            $this->payload = $this->getPayload($id);
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();
    }

    function getPayload($id = null)
    {

        $hold = $this->getStats();
        if (is_null($id)) {
            $page = $this->getPages();
            if ($page->isEmpty()) return $this->emptyData();
        } else {
            $page = $this->getPage($id);
            if (is_null($page)) return $this->emptyData();
        }

        if (is_a($page, 'Illuminate\Database\Eloquent\Collection')) {
            $this->ids = $page->pluck('id');
        } else {
            $this->ids[] = $page->id;
        }

        $hold['pages'] = $this->buildArray($page);
        $hold['sitemap'] = $this->getSitemap();
        if ($this->category_status) {
            $hold['category'] = $this->getCategory($page);
        }
        if ($this->criteria_status) {
            $hold['criteria'] = $this->getPageCriteria();
        }
        return $hold;
    }

    private function getPages()
    {
        $order_type = $this->getOrder();
        $query_type = $this->getQuery('pageQuery');
        $pages = Page::whereHas('details')
            ->select('id', 'sitemap_id', 'status', 'order', 'date', 'ctex_1', 'ctex_2', 'cint_1', 'cint_2', 'cint_3', 'cint_4', 'cint_5', 'cvar_1', 'cvar_2','cvar_3','cvar_4','cvar_5','cdec_1','cdec_2','cdec_3')
            ->with(['extras', 'details' => function ($q) {
                $q->where('language_id', $this->language['id'])
                    ->with('extras', 'url');
            }]);

        foreach ($query_type as $query) {
            $pages = call_user_func_array([$pages, array_shift($query)], $query);
        }

        foreach ($order_type as $key => $type) {
            if ($key == 'random') {
                $pages = $pages->inRandomOrder();
            } else {
                $pages = $pages->orderBy($key, $type);
            }
        }

        $pages = $pages->get();
        return $pages;
    }

    private function getPage($id)
    {
        return Page::whereHas('details')
            ->select('id', 'sitemap_id', 'status', 'order', 'date',  'ctex_1', 'ctex_2', 'cint_1', 'cint_2', 'cint_3', 'cint_4', 'cint_5', 'cvar_1', 'cvar_2','cvar_3','cvar_4','cvar_5','cdec_1','cdec_2','cdec_3')
            ->with(['extras', 'details' => function ($q) {
                $q->where('language_id', $this->language['id'])
                    ->with('extras', 'url');
            }])
            ->find($id);
    }

    private function getSitemap()
    {

        $sitemap = Sitemap::whereHas('pages', function ($q) {
            $q->whereIn('id', $this->ids);
        })
            ->select('id', 'sitemap_type_id', 'feature_tag', 'ctex_1', 'ctex_2', 'cint_1', 'cint_2', 'cvar_1', 'cvar_2')
            ->with(['extras', 'details' => function ($q) {
                $q->where('language_id', $this->language['id'])
                    ->with('extras', 'url');
            }])->first();

        return $this->buildArray($sitemap);
    }

    private function getCategory($page)
    {
        $query_type = $this->getQuery('categoryQuery');


        if (count($this->ids) > 1) {
            $categories = Category::whereHas('pages', function ($q) {
                $q->whereIn('id', $this->ids);
            });
        } else {
            $categories = $page->categories();
        }

        $categories = $categories->select('id', 'sitemap_id', 'status', 'lft', 'rgt', 'categories.category_id', 'depth', 'ctex_1', 'ctex_2', 'cint_1', 'cint_2', 'cvar_1', 'cvar_2')
            ->orderBy('lft')
            ->with(['extras', 'details' => function ($q) {
                $q->where('language_id', $this->language['id'])
                    ->with('extras', 'url');
            }]);

        foreach ($query_type as $query) {
            $categories = call_user_func_array([$categories, array_shift($query)], $query);
        }

        $categories = $categories->get();

        return $this->buildArray($categories);
    }

    private function getPageCriteria()
    {

        $criterias = Criteria::whereHas('pages', function ($q) {
            $q->whereIn('id', $this->ids);
        })
            ->with(['details' => function ($q) {
                $q->where('language_id', $this->language['id']);
            }, 'parent' => function ($query) {
                $query->with(['details' => function ($q) {
                    $q->where('language_id', $this->language['id']);
                }]);
            }])
            ->get();

        if ($criterias->count() <= 0) {
            return $this->emptyData();
        }
        $hold = array();

        foreach ($criterias as $criteria) {
            $detail_name = $criteria->details()->first()->name;
            $parent_detail_name = $criteria->parent->details()->first()->name;
            $hold[$parent_detail_name] = $detail_name;
        }
        return $hold;
    }
}


