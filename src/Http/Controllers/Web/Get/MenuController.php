<?php

namespace Mediapress\API\Http\Controllers\Web\Get;

use Mediapress\API\Http\Controllers\Web\ApiController;

use Mediapress\Modules\Content\Models\Menu;
use Mediapress\Modules\Content\Models\MenuDetail;

class MenuController extends ApiController
{
    private $parent_id = [
        'Mediapress\Modules\Content\Models\PageDetail' => [
            'parent' => "page_id",
            'key' => "page"
        ],
        'Mediapress\Modules\Content\Models\SitemapDetail' => [
            'parent' => "sitemap_id",
            'key' => "sitemap"
        ],
        'Mediapress\Modules\Content\Models\CategoryDetail' => [
            'parent' => "category_id",
            'key' => "category"
        ],
    ];

    public function index()
    {
        $this->type = 'menu';
        try {
            $this->status = true;
            $this->code = 200;
            $this->payload = $this->getPayload(null);
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();
    }

    function getPayload($id)
    {
        $return = $this->getMenu();
        if(count($return) <= 0)
            return $this->emptyData();
        return $return;
    }

    private function getMenu()
    {
        $lang_id = $this->language['id'];
        return \Cache::remember($this->cacheKey([$lang_id, "MENU", $this->status]), 60, function () use ($lang_id) {
            $main_menus = $this->getMainMenu($lang_id);
            $hold = array();
            foreach ($main_menus as $main_menu) {
                $temp = [];
                $menu_details = MenuDetail::where('status', 1)
                    ->where('language_id', $lang_id)
                    ->where('menu_id', $main_menu->id)
                    ->orderBy('lft')
                    ->get();
                $cluster_menus = cluster($menu_details, ["parent_id" => "parent"]);
                $temp = $this->getMenuArray($cluster_menus);
                $temp = count($temp) > 1 ? $temp : array_shift($temp);
                $hold[$main_menu->slug] = $temp;

            }
            return $hold;
        });
    }
    private function getMainMenu($lang_id)
    {
        return Menu::whereHas('details', function ($q) use ($lang_id) {
            $q->where('language_id', $lang_id);
        })
            ->with(['details' => function ($q) use ($lang_id) {
                $q->where('language_id', $lang_id)
                    ->where('status', 1);
            }])
            ->get();
    }
    private function getMenuArray($main_menus)
    {
        $hold = array();
        foreach ($main_menus as $menu) {
            $menu_link = $this->getUrl($menu);

            $menu_temp = [
                'id' => $menu->id,
                'model' => $this->getModel($menu),
                'name' => strip_tags($menu->name),
                'url' => $menu_link,
                'target' => !is_null($menu->target) ? $menu->target : null,
                'children' => null,
            ];
            if ($menu->children->isNotEmpty()) {
                $menu_temp['children'] = $this->getMenuArray($menu->children);
            }
            $hold[] = $menu_temp;
        }
        return $hold;
    }
    private function getModel($menu)
    {
        $model_id = null;
        $model_key = null;
        if ($menu->url) { //İç Link İse
            $model_detail = $menu->url->model_type::find($menu->url->model_id);
            if ($model_detail) {
                $parent_id_key = $this->parent_id[$menu->url->model_type]['parent'];
                $model_id = $model_detail->$parent_id_key;
                $model_key = $this->parent_id[$menu->url->model_type]['key'];
            }
        }
        return [
            'id' => $model_id,
            'key' => $model_key,
        ];
    }
    private function getUrl($data)
    {
        if ($data->url) {
            $url = $data->url->url;
        } elseif ($data->type == 2) {
            $url = $data->out_link;
        } else {
            $url = 'javascript:void(0)';
        }

        return $url;
    }
}


