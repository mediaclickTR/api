<?php

namespace Mediapress\API\Http\Controllers\Web\Get;

use Mediapress\API\Http\Controllers\Web\ApiController;

use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\Criteria;

class CategoryController extends ApiController
{

    private $ids = array();

    public function index($id = null)
    {
        $this->type = 'category';
        try {
            $this->status = true;
            $this->code = 200;
            $this->payload = $this->getPayload($id);
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();
    }


    function getPayload($id = null)
    {
        $hold = $this->getStats();
        if (is_null($id)) {
            $category = $this->getCategories();
            if ($category->isEmpty()) return $this->emptyData();
        } else {
            $category = $this->getCategory($id);
            if (is_null($category)) return $this->emptyData();
        }

        if (is_a($category, 'Illuminate\Database\Eloquent\Collection')) {
            $this->ids = $category->pluck('id');
        } else {
            $this->ids[] = $category->id;
        }

        $hold['category'] = $this->buildArray($category);
        if ($this->sitemap_status) {
            $hold['sitemap'] = $this->getSitemap();
        }
        if ($this->page_status) {
            $hold['pages'] = $this->getPages($category);
        }
        if ($this->criteria_status) {
            $hold['criteria'] = $this->getCategoryCriteria();
        }
        if (request()->get('all') == 1) {
            $hold['all'] = $this->getCategoryCluster($category);
        }

        return $hold;
    }

    private function getCategories()
    {
        $query_type = $this->getQuery('categoryQuery');
        $categories = Category::select('id', 'sitemap_id', 'category_id', 'ctex_1', 'ctex_2', 'cint_1', 'cint_2', 'cvar_1', 'cvar_2')
            ->whereHas('details')
            ->with(['extras', 'details' => function ($q) {
                $q->where('language_id', $this->language['id'])
                    ->select('id', 'category_id', 'name', 'detail')
                    ->with('extras', 'url');
            }]);

        foreach ($query_type as $query) {
            $categories = call_user_func_array([$categories, array_shift($query)], $query);
        }
        $categories = $categories->get();
        return $categories;
    }

    private function getCategory($id)
    {
        return Category::where('id', $id)
            ->select('id', 'sitemap_id', 'category_id', 'ctex_1', 'ctex_2', 'cint_1', 'cint_2', 'cvar_1', 'cvar_2')
            ->whereHas('details')
            ->with(['extras', 'details' => function ($q) {
                $q->where('language_id', $this->language['id'])
                    ->select('id', 'category_id', 'name', 'detail')
                    ->with('extras', 'url');
            }])
            ->first();
    }

    private function getSitemap()
    {
        $sitemap = Sitemap::whereHas('categories', function ($q) {
            $q->whereIn('id', $this->ids);
        })
            ->select('id', 'sitemap_type_id', 'feature_tag', 'ctex_1', 'ctex_2', 'cint_1', 'cint_2', 'cvar_1', 'cvar_2')
            ->with(['extras', 'details' => function ($q) {
                $q->where('language_id', $this->language['id'])
                    ->with('extras', 'url');
            }])->get();

        return $this->buildArray($sitemap);
    }

    private function getPages($category)
    {
        $order_type = $this->getOrder();
        $query_type = $this->getQuery('pageQuery');

        if (count($this->ids) > 1) {
            $pages = Page::whereHas('categories', function ($q) {
                $q->whereIn('id', $this->ids);
            });
        } else {
            $pages = $category->pages();
        }
        $pages = $pages->select('id', 'sitemap_id', 'order', 'date', 'status', 'ctex_1', 'ctex_2', 'cint_1', 'cint_2', 'cvar_1', 'cvar_2')
            ->where('status', 1)
            ->with(['extras', 'details' => function ($q) {
                $q->where('language_id', $this->language['id'])
                    ->with('extras', 'url');
            }]);


        foreach ($query_type as $query) {
            $pages = call_user_func_array([$pages, array_shift($query)], $query);
        }

        foreach ($order_type as $key => $type) {
            if ($key == 'random') {
                $pages = $pages->inRandomOrder();
            } else {
                $pages = $pages->orderBy($key, $type);
            }
        }

        if ($this->pagination_status) {
            $pages = $pages->paginate($this->per_page, [], 'p');

            $new_path = str_replace(["&p=" . request()->get('p'), "p=" . request()->get('p')], "", request()->getRequestUri());
            $pages->withPath(url($new_path));
            $items = $pages->getCollection();
            $pages_arr = $pages->toArray();


            $this->setCategoryIdForPages($items);

            unset($pages_arr['data']);
            return [
                'paginate' => $pages_arr,
                'content' => $this->buildArray($items)
            ];

        } else {

            $pages = $pages->get();

            $this->setCategoryIdForPages($pages);


            return [
                'paginate' => false,
                'content' => $this->buildArray($pages)
            ];
        }
    }

    private function getCategoryCluster($current_category)
    {
        if ($current_category) {
            $sitemap_id = $current_category->sitemap_id;

            $categories = Category::where('sitemap_id', $sitemap_id)
                ->select('id', 'sitemap_id', 'category_id', 'ctex_1', 'ctex_2', 'cint_1', 'cint_2', 'cvar_1', 'cvar_2')
                ->orderBy('lft')
                ->where('category_id', null)
                ->with(['extras', 'details' => function ($q) {
                    $q->where('language_id', $this->language['id'])
                        ->with('extras', 'url');
                }, 'children' => function ($q) {
                    $q->with(['extras', 'details' => function ($que) {
                        $que->where('language_id', $this->language['id'])
                            ->with('extras', 'url');
                    }])->orderBy('lft');
                }])
                ->get();

            return $this->setClusterArray($categories);
        } else {
            return array();
        }
    }

    private function setClusterArray($categories)
    {
        $hold = array();
        foreach ($categories as $category) {
            if (is_null($category->details->first())) continue;
            $category_temp = [
                'id' => $category->id,
                'name' => $category->details->first()->name,
                'url' => $category->details->first()->url->url,
            ];
            if ($category->children->isNotEmpty()) {
                $category_temp['children'] = $this->setClusterArray($category->children);
            }
            $hold[] = $category_temp;
        }
        return $hold;
    }

    private function getCategoryCriteria()
    {

        $criterias = Criteria::whereHas('categories', function ($q) {
            $q->whereIn('id', $this->ids);
        })
            ->where('criterias.criteria_id', null)
            ->with(['details' => function ($q) {
                $q->where('language_id', $this->language['id']);
            }, 'children' => function ($query) {
                $query->with(['details' => function ($q) {
                    $q->where('language_id', $this->language['id']);
                }]);
            }])
            ->get();

        if ($criterias->count() <= 0) {
            return $this->emptyData();
        }
        $hold = array();

        foreach ($criterias as $criteria) {
            $detail_name = $criteria->details()->first()->name;
            foreach ($criteria->children as $child) {
                $child_detail_name = $child->details()->first()->name;
                $hold[$detail_name][] = $child_detail_name;
            }
        }
        return $hold;
    }

    private function setCategoryIdForPages(&$pages)
    {
        if (count($this->ids) > 1) {
            $pages->map(function ($pag) {
                $pag['category_id'] = $pag->categories->pluck('id');
                $pag->unsetRelation('categories');
                return $pag;
            });
        }
    }


}


