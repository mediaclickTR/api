<?php

namespace Mediapress\API\Http\Controllers\Web\Get;

use Mediapress\API\Http\Controllers\Web\ApiController;

use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Scene;
use Mediapress\Modules\Content\Models\Slider;
use Mediapress\Modules\Content\Models\Website;

class SliderController extends ApiController
{

    public function index($id = null)
    {
        $this->type = 'slider';
        try {
            $this->status = true;
            $this->code = 200;
            $this->payload = $this->getPayload($id);
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();
    }

    function getPayload($id)
    {
        if (!$id) {
            return $this->getSliders();
        }
        return $this->getScenes($id);
    }

    private function getSliders()
    {
        $sliders = Slider::all();
        if ($sliders->count() <= 0) return $this->emptyData();
        $hold = array();
        foreach ($sliders as $slider) {
            $hold[] = [
                'id' => $slider->id,
                'name' => $slider->name,
                'type' => $slider->type,
                'count' => [
                    'text' => $slider->text,
                    'button' => $slider->button,
                    'screen' => $slider->screen,
                    'width' => $slider->width,
                    'height' => $slider->height,
                ]
            ];
        }
        return $hold;
    }

    private function getScenes($id)
    {

        $scenes = Scene::where('slider_id', $id)
            ->where('status', 1)
            ->orderBy('order')
            ->whereHas('details', function ($q) {
                $q->where('language_id', $this->language['id']);
            })
            ->with(['details' => function ($q) {
                $q->where('language_id', $this->language['id']);
            }])
            ->get();

        if ($scenes->count() <= 0) return $this->emptyData();
        $hold = array();
        foreach ($scenes as $scene) {
            $detail = $scene->details->first();
            $hold[] = [
                'time' => $scene->time,
                'texts' => $detail->texts,
                'buttons' => $detail->buttons,
                'files' => $this->setImages($detail->files),
                'urls' => $detail->url
            ];
        }
        return $hold;
    }

    private function setImages($files)
    {
        $hold = array();
        foreach ($files as $key => $file) {
            if ($key == 'tablet' && $file == '[]') {
                $image = image(collect(json_decode($files['desktop'], true))->first())->resize(['w' => 800]);
            } elseif ($key == 'mobile' && $file == '[]') {
                $image = image(collect(json_decode($files['desktop'], true))->first())->resize(['w' => 400]);
            } else {
                $image = collect(json_decode($file, true))->first() ? image(collect(json_decode($file, true))->first()) : null;
            }
            unset($image->model);
            $hold[$key] = $image;
        }
        return $hold;
    }

}


