<?php

namespace Mediapress\API\Http\Controllers\Web\Get;

use Mediapress\API\Http\Controllers\Web\ApiController;

use Mediapress\FileManager\Models\MDisk;

class DiskKeyController extends ApiController
{

    public function index()
    {
        $this->type = 'disk_key';
        try {
            $this->status = true;
            $this->code = 200;
            $this->payload = $this->getPayload(null);
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();
    }

    function getPayload($id)
    {
        return MDisk::get(['diskkey', 'driver']);
    }

}


