<?php

namespace Mediapress\API\Http\Controllers\Web\Get;

use Mediapress\API\Http\Controllers\Web\ApiController;


use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\Criteria;
use Mediapress\Modules\Content\Models\SitemapDetail;

class SitemapController extends ApiController
{

    public function index($id)
    {
        try {
            $this->status = true;
            $this->code = 200;
            $this->payload = $this->getPayload($id);
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();
    }

    function getPayload($id = null)
    {
        $sitemap_detail = $this->getSitemapDetail($id);
        if (is_null($sitemap_detail) || is_null($sitemap_detail->sitemap)) return $this->emptyData();

        $this->type = 'sitemap';

        $hold = $this->getStats();

        $hold['sitemap'] = $this->getSitemap($id);
        if ($this->page_status) {
            $hold['pages'] = $this->getSitemapPages($id);
        }
        if ($this->category_status) {
            $hold['category'] = $this->getSitemapCategory($id);
        }
        if ($this->criteria_status) {
            $hold['criteria'] = $this->getSitemapCriteria($id);
        }

        return $hold;
    }


    private function getSitemap($id)
    {

        $sitemap = Sitemap::where('id', $id)
            ->select('id', 'sitemap_type_id', 'feature_tag', 'ctex_1', 'ctex_2', 'cint_1', 'cint_2', 'cvar_1', 'cvar_2')
            ->with(['extras', 'details' => function ($q) {
                $q->where('language_id', $this->language['id'])
                    ->with('extras', 'url');
            }])
            ->first();
        if ($sitemap)
            return $this->buildArray($sitemap);
        return array();
    }

    private function getSitemapDetail($id)
    {
        return SitemapDetail::where('sitemap_id', $id)
            ->where('language_id', $this->language['id'])
            ->with('sitemap', 'url')
            ->first();
    }

    private function getSitemapCategory($id)
    {
        $query_type = $this->getQuery('categoryQuery');
        $categories = Category::where('sitemap_id', $id)
            ->select('id', 'sitemap_id', 'status', 'lft', 'rgt', 'depth', 'category_id', 'ctex_1', 'ctex_2', 'cint_1', 'cint_2', 'cvar_1', 'cvar_2')
            ->orderBy('lft')
            ->where(function ($q){
                $q->where('category_id',null)->orWhere('category_id',0);
            })
            ->with(['extras', 'details' => function ($q) {
                $q->where('language_id', $this->language['id'])
                    ->with('extras', 'url');
            }, 'children' => function ($q) {
                $q->with(['extras', 'details' => function ($que) {
                    $que->where('language_id', $this->language['id'])
                        ->with('extras', 'url');
                }])->orderBy('lft');
            }]);

        foreach ($query_type as $query) {
            $categories = call_user_func_array([$categories, array_shift($query)], $query);
        }

        $categories = $categories->get();

        if ($categories)
            return $this->setCategoryCluster($categories);
        return array();
    }

    private function setCategoryCluster($categories)
    {
        $hold = array();
        foreach ($categories as $category) {

            $category_temp = [
                'id' => $category->id,
                'name' => $category->details->first()->name,
                'url' => $category->details->first()->url->url,
            ];
            if ($category->children->isNotEmpty()) {
                $category_temp['children'] = $this->setCategoryCluster($category->children);
            }
            $hold[] = $category_temp;
        }
        return $hold;
    }

    private function getSitemapPages($id)
    {
        $order_type = $this->getOrder();
        $query_type = $this->getQuery('pageQuery');
        $pages = Page::where('sitemap_id', $id)
            ->select('id', 'sitemap_id', 'order', 'date', 'status', 'ctex_1', 'ctex_2', 'cint_1', 'cint_2', 'cint_3', 'cint_4', 'cint_5', 'cvar_1', 'cvar_2','cvar_3','cvar_4','cvar_5','cdec_1','cdec_2','cdec_3')
            ->where('status', 1)
            ->with(['extras', 'details' => function ($q) {
                $q->where('language_id', $this->language['id'])
                    ->with('extras', 'url');
            }]);

        if ($this->page_category_status) {

            $pages = $pages->with(['categories' => function ($q) {
                $q->select('categories.id', 'categories.category_id', 'categories.lft', 'categories.status', 'categories.ctex_1', 'categories.ctex_2', 'categories.cint_1', 'categories.cint_2', 'categories.cvar_1', 'categories.cvar_2')
                    ->with(['details' => function ($q) {
                        $q->where('language_id', $this->language['id'])
                            ->with('extras', 'url');
                    }]);
            }]);
        }

        foreach ($query_type as $query) {

            $pages = call_user_func_array([$pages, array_shift($query)], $query);

        }


        foreach ($order_type as $key => $type) {
            if ($key == 'random') {
                $pages = $pages->inRandomOrder();
            } else {
                $pages = $pages->orderBy($key, $type);
            }
        }

        if ($this->pagination_status) {
            $pages = $pages->paginate($this->per_page, [], 'p');

            $new_path = str_replace(["&p=" . request()->get('p'), "p=" . request()->get('p')], "", request()->getRequestUri());
            $pages->withPath(url($new_path));
            $items = $pages->getCollection();
            $pages_arr = $pages->toArray();


            unset($pages_arr['data']);
            return [
                'paginate' => $pages_arr,
                'content' => $this->buildArray($items)
            ];

        } else {
            $pages = $pages->get();

            return [
                'paginate' => false,
                'content' => $this->buildArray($pages)
            ];
        }
    }

    private function getSitemapCriteria($id)
    {

        $criterias = Criteria::where('sitemap_id', $id)
            ->where('criteria_id', null)
            ->with(['details' => function ($q) {
                $q->where('language_id', $this->language['id']);
            }, 'children' => function ($query) {
                $query->with(['details' => function ($q) {
                    $q->where('language_id', $this->language['id']);
                }]);
            }])
            ->get();

        if ($criterias->count() <= 0) {
            return $this->emptyData();
        }
        $hold = array();

        foreach ($criterias as $criteria) {
            $detail_name = $criteria->details()->first()->name;
            foreach ($criteria->children as $child) {
                $child_detail_name = $child->details()->first()->name;
                $hold[$detail_name][] = $child_detail_name;
            }
        }
        return $hold;
    }
}


