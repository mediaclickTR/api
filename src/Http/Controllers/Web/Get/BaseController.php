<?php

namespace Mediapress\API\Http\Controllers\Web\Get;

use Mediapress\API\Http\Controllers\Web\ApiController;

use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Website;

class BaseController extends ApiController
{

    public function index()
    {
        $this->type = 'base';
        try {
            $this->status = true;
            $this->code = 200;
            $this->payload = $this->getPayload(null);
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();
    }

    function getPayload($id)
    {
        $mediapress = new Mediapress();
        return \Cache::remember($this->cacheKey(["BASE", $this->status]), 60, function () use ($mediapress) {
            return [
                'website' => $this->getWebsite($mediapress),
                'default_language' => $this->getDefaultLanguage(),
                'other_languages' => $this->getOtherLanguage(),
            ];
        });
    }
    private function getWebsite($mediapress) {
        return [
            'domain' => $mediapress->website->slug,
            'ssl' => $mediapress->website->ssl,
        ];
    }
    private function getOtherLanguage() {
        $active_languages = Website::with('languages')->first();

        $hold = array();
        foreach ($active_languages->languages as $language) {
            $hold[] = [
                'id' => $language->id,
                'code' => $language->code,
                'name' => $language->name,
                'native' => $language->native,
                'regional' => $language->regional,
            ];
        }
        return $hold;
    }
}


