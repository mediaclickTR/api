<?php

namespace Mediapress\API\Http\Controllers\Web\Get;

use Mediapress\API\Http\Controllers\Web\ApiController;

use Mediapress\Modules\Content\Models\SocialMedia;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\MPCore\Models\CountryGroup;

class SocialMediaController extends ApiController
{
    public function index($group_id)
    {
        $this->type = 'social-media';
        try {
            $this->status = true;
            $this->code = 200;
            $this->payload = $this->getPayload($group_id);
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();
    }

    function getPayload($group_id)
    {
        $return = $this->getSocialMedia($group_id*1);
        if(count($return) <= 0)
            return $this->emptyData();
        return $return;
    }

    function getSocialMedia($group_id) {

        $website_id = request()->get('website_id') ?: Website::where('default', 1)->first()->id;
        $country_group_id = request()->get('country_group') ?: CountryGroup::where('owner_id', $website_id)->first()->id;
        $social_medias = SocialMedia::where('group_id', $group_id)->where('website_id', $website_id)
            ->orderBy('order')
            ->where('status', '1')
            ->get();

        $hold = array();
        foreach ($social_medias as $social_media) {
            $hold[] = [
                'id' => $social_media->id,
                'website_id' => $social_media->website_id,
                'link' => $social_media->link[$country_group_id][$this->language['id']] ?? "",
                'icon' => image($social_media->icon)
            ];
        }

        return $hold;

    }

}


