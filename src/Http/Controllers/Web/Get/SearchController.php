<?php

namespace Mediapress\API\Http\Controllers\Web\Get;

use Mediapress\API\Http\Controllers\Web\ApiController;


use Mediapress\Modules\MPCore\Models\Search;

class SearchController extends ApiController
{

    public function index($key)
    {
        $this->type = 'search';
        try {
            $this->status = true;
            $this->code = 200;
            $this->payload = $this->getPayload($key);
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();
    }

    function getPayload($key)
    {
        $query = strip_tags($key);
        return $this->search($query);
    }

    private function search($query)
    {
        $searchResults = searchQuery($query, $this->per_page, $this->language['id']);

        $new_path = str_replace(["&p=".request()->get('p'), "p=".request()->get('p')], "", request()->getRequestUri());
        $searchResults->withPath(url($new_path));
        $items = $searchResults->getCollection();
        $pages_arr = $searchResults->toArray();

        unset($pages_arr['data']);
        return [
            'paginate' => $pages_arr,
            'content' => $this->buildSearchArray($items)
        ];
    }
}


