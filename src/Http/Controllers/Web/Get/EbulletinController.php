<?php

namespace Mediapress\API\Http\Controllers\Web\Get;

use Mediapress\API\Http\Controllers\Web\ApiController;

use Mediapress\Modules\Heraldist\Models\Ebulletin;
use Mediapress\Modules\Heraldist\Mail\EbulletinSender;

class EbulletinController extends ApiController
{

    public function index($code = null)
    {
        $this->type = 'e-bulletin';
        try {
            $this->status = true;
            $this->code = 200;
            $this->payload = $this->getPayload($code);
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();
    }

    function getPayload($code)
    {
        if($code) {
            return $this->activateUser($code);
        } else {
            return $this->registerUser();
        }
    }

    private function  registerUser() {

        $request = request()->all();
        $fields = $this->getFields();
        $rules = $this->getRules();
        if(count($request) == 0) {
            return $this->emptyData();
        }
        $validator = \Validator::make($request, $rules, [], $fields);

        if($validator->fails()) {

            $this->status = false;
            $this->code = 503;
            $hold['messages'] = $validator->errors()->all();

        } else {
            $user = $this->createUser();
            \Mail::to($user->email)->send(new EBulletinSender($user, 'api/ebulletin'));
            $this->status = true;
            $this->code = 200;
            $hold['messages'][] = langPart('api.ebulletin_create_success', "E-bülten kaydı tamamlanmıştır.", [], $this->language['id']);
        }
        return $hold;
    }

    private function activateUser($code) {

        $id = decrypt($code);
        $user = EBulletin::find($id);

        if (!$user || $user->activated == 1) {
            return $this->emptyData();
        }
        $user->activated = 1;
        $user->save();

        $hold['messages'][] = langPart('api.ebulletin_activate_success', "E-bülten aktivasyonu tamamlanmıştır.", [], $this->language['id']);

        return $hold;
    }


    private function createUser() {
        $website_id = isset(session()->get('panel')['website']) ? session()->get('panel')['website']->id : 1;
        $user = Ebulletin::create(
            [
                'website_id' => $website_id,
                'name' => request()->get('name'),
                'email' => request()->get('email'),
                'phone' => request()->get('phone'),
            ]
        );
        return $user;
    }


    private function getFields() {
        return [
            'email' => 'E-posta',
            'name' => 'Ad Soyad',
            'phone' => 'Telefon'
        ];
    }

    private function getRules() {
        return [
            'email' => 'required|email|unique:e_bulletins,email,NULL,id,deleted_at,NULL',
            'name' => 'filled|min:3',
            'phone' => 'filled|min:3'
        ];
    }
}


