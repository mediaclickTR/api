<?php

namespace Mediapress\API\Http\Controllers\Web\Get;

use Mediapress\API\Http\Controllers\Web\ApiController;

use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\MPCore\Models\Url;

class UrlController extends ApiController
{
    private $class_name = [
        'Mediapress\Modules\Content\Models\Page' => 'page',
        'Mediapress\Modules\Content\Models\Sitemap' => 'sitemap',
        'Mediapress\Modules\Content\Models\Category' => 'category',
    ];

    public function index()
    {
        $this->type = "url";
        try {
            $this->status = true;
            $this->code = 200;
            $this->payload = $this->getPayload(null);
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();
    }

    function getPayload($id)
    {
        $query = request()->get('q');

        return $this->getModel($query);
    }

    private function getModel($query)
    {
        $url = Url::where('type', 'original')
            ->where('url', $query)
            ->first();

        if($url && $url->model) {

            $detailModel = $url->model;
            $parentModel = $detailModel->parent;
            $model_type = $this->class_name[get_class($parentModel)] ?? null;

            if($parentModel && !is_null($model_type)) {
                $language = $detailModel->language;

                if($parentModel->sitemap) { //page or category
                    $sitemap_type = $parentModel->sitemap->sitemapType->name;
                } else { //sitemap
                    $sitemap_type = $parentModel->sitemapType->name;
                }
                return [
                    'request_uri' => $query,
                    'model_type' => $model_type,
                    'model_id' => $parentModel->id,
                    'sitemap_type' => $sitemap_type,
                    'language' => [
                        'id' => $language->id,
                        'code' => $language->code,
                        'name' => $language->name
                    ],
                    'metas' => $url->metaWorker->setUrlBase(config('mediapress.vue_url'))->toVueClass()
                ];
            }

        }









        return $this->emptyData();
    }
}


