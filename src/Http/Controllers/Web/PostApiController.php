<?php

namespace Mediapress\API\Http\Controllers\Web;


use Mediapress\Modules\Content\Models\Meta;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\Modules\MPCore\Facades\URLEngine;
use \Mediapress\Modules\MPCore\Models\Language;
use \Mediapress\Modules\MPCore\Models\CountryGroup;
use \Mediapress\Modules\Content\Models\LanguageWebsite;
use Mediapress\Modules\MPCore\Models\Url;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Auth\Models\Admin;

class PostApiController
{
    protected $status = true;
    protected $payload;
    protected $language;
    protected $type = "";
    protected $code = 200;

    protected $website = null;
    protected $country_group = null;
    protected $user = null;

    public function __construct()
    {
        $this->setCurrentLanguage();
        $this->setWebsite();
        $this->setCountryGroup();
        $this->setUser();

    }

    protected function checkAttributes()
    {
        if (is_null($this->language) || is_null($this->website) || is_null($this->country_group)) {
            throw new \Exception("Country Group does not set! Check sitemap of languages! for ". request()->get('language'));
        }

        if(is_null(request()->get('api_token')) || is_null($this->user)) {
            throw new \Exception("User not found !");
        }
        return true;
    }

    protected function getError(\Exception $exception)
    {
        return [
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
        ];
    }

    protected function emptyData($messages = null)
    {
        $this->code = 404;
        return [
            'message' => count($messages) > 0 ? $messages : "Data Not Found"
        ];
    }

    protected function setJson()
    {
        $hold = [
            'success' => $this->status,
            'code' => $this->code,
            'api_token' => $this->user ? $this->user->api_token : request()->get('api_token'),
            'response' => [
                'current_language' => [
                    'id' => $this->language ? $this->language->id : "000",
                    'code' => $this->language ? $this->language->code : request()->get('language')
                ],
                'type' => $this->type,
                'data' => $this->payload
            ]
        ];
        return response()->json($hold, $this->code)->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
            ->header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, X-Token-Auth, Authorization');
    }

    public function setUrl(array &$data, $detail_type)
    {
        $website_id = $data['website_id'] * 1;
        $language_id = $this->language['id'] * 1;
        $country_group_id = $data['country_group_id'];
        $parent_id = $data["parent_id"] * 1;


        $cls_rel_data = getSimpleRelationData($detail_type, "Detail");
        $parent_type = $cls_rel_data["parent_class"];


        $slug = $data['name'] ?: null;


        $country_group_model = CountryGroup::find($country_group_id);
        $detail_model = $detail_type::where($cls_rel_data["childs_foreign_key"], $parent_id)->where("language_id", $language_id)
            ->where("country_group_id", $country_group_model ? $country_group_model->id : null)->first();
        $parent_model = $parent_type::find($parent_id);
        if ($detail_model == null) {
            if (is_a($parent_model, Sitemap::class)) {
                $detail_data = [
                    "type" => $detail_type,
                    "page" => $parent_model,
                    "sitemap" => $parent_model,
                    "website" => $parent_model->websites->first()
                ];
                if ($website_id == 0) {
                    $website_id =
                        isset($detail_data["website"])
                            ? $detail_data["website"]->id
                            : $parent_model
                            ->websites()
                            ->first()
                            ->id;
                }
            } else {
                $detail_data = [
                    "type" => $detail_type,
                    "page" => $parent_model,
                    "sitemap" => $parent_model->sitemap,
                    "website" => $parent_model->sitemap->websites->first()
                ];
                if ($website_id == 0) {
                    $website_id = isset($detail_data["website"]) ? $detail_data["website"]->id : $parent_model->sitemap->websites()->first()->id;
                }
            }
        }

        $pattern = URLEngine::generateUrlString(($detail_model ?? $detail_data), $country_group_model, $language_id, $slug);
        $data['slug'] = URLEngine::checkPattern($website_id, $pattern["pattern"], $pattern["slug"])['slug'];
    }

    private function setWebsite()
    {
        if (app()->runningInConsole()) {
            $this->website = Website::first();
        } else {
            $this->website = Website::where('slug', request()->server('HTTP_HOST'))->first();
        }
    }

    private function setUser()
    {
        $apiToken = request()->get('api_token');
        if($apiToken) {
            $this->user = Admin::where('api_token', strtolower($apiToken))->first();
            session(["panel.user" => $this->user]);
        }
    }

    protected function setCountryGroup()
    {
        if($this->language) {
            $this->country_group = CountryGroup::where("owner_id", $this->website->id)->whereHas('languages', function ($q) {
                $q->where("id", $this->language->id);
            })->first();
        }
    }

    protected function setCurrentLanguage()
    {
        if (request()->get('language')) {
            $language_code = request()->get('language');
            $language = Language::where('code', strtolower($language_code))->select('id', 'code', 'name', 'regional')->first();
        } else {
            $language = $this->getDefaultLanguage();
        }

        $this->language = $language;
    }

    protected function getDefaultLanguage()
    {
        if (app()->runningInConsole()) {
            $default_language = CountryGroup::where("code", "gl")->first()
                ->languages()->where('country_group_language.default', 1)
                ->first();
        } else {
            $domain = request()->server('HTTP_HOST');
            $region = Website::where('slug', $domain)->first()->regions;
            $language = Language::where('code', "tr")->first();

            if ($region == 0) {
                $country_group = CountryGroup::where("code", "gl")->whereHas('languages', function ($q) use ($language) {
                    $q->where("id", $language->id);
                })->first();
            } else {
                $ipDetails = $this->ip_details();
                if ($ipDetails) {
                    $ipDetails = $ipDetails["country"]["iso_code"];
                }
                $country_group = CountryGroup::whereHas("countries", function ($q) use ($ipDetails) {
                    $q->where("code", $ipDetails);
                })->whereHas('languages', function ($q) use ($language) {
                    $q->where("id", $language->id);
                })->first();
            }

            if($country_group) {
                $default_language = $country_group->languages()->where('country_group_language.default', 1)->first();
            } else {
                $default_language = CountryGroup::where("code", "gl")->first()
                    ->languages()->where('country_group_language.default', 1)
                    ->first();
            }
        }

        return $default_language;
    }

    protected function getMeta($url)
    {
        $url = Url::where('url', $url)->first();
        if ($url) {
            $render = $url->metas->getListForDevice();
            return preg_replace("/<link rel=\"canonical\" href=\"(.*)+(\n)?/m", '', $render);
        }
    }

    protected function ip_details($ip = null)
    {
        if (!$ip) {
            if (request()->server('HTTP_CF_CONNECTING_IP')) {
                $ip = request()->server('HTTP_CF_CONNECTING_IP');
            } else {
                $ip = request()->server('REMOTE_ADDR');
            }
        }
        return json_decode(file_get_contents('https://geoipmediaclick.appspot.com/?ip=' . $ip), true);
    }

    protected function buildArray($data)
    {
        if (is_a($data, "Illuminate\Database\Eloquent\Collection")) {
            $return = [];
            foreach ($data as $key => $page) {
                $return[$page->id] = $this->buildArray($page);
            }
            return $return;
        } else {
            return $this->getSingleArray($data);
        }
    }

    private function getSingleArray($page)
    {
        $temp = $page->toArray();
        $categories = collect();
        if (isset($temp['categories'])) {
            unset($temp['categories']);
            $categories = $page->categories;
        }
        $return = [];
        foreach ($temp as $key => $value) {
            if (is_array($value)) {
                $return += $this->getIdentity($key, $value);
            } else {
                $return[$key] = $value;
            }
        }
        if ($page->details && $page->details->first() && $page->details->first()->url) {
            $return['metas'] = $this->getMeta($page->details->first()->url->url);
        }
        if ($page->mfiles) {
            foreach ($page->mfiles as $file) {
                if (request()->get('w')) {
                    $image_temp = image($file)->resize(['w' => request()->get('w')]);
                } else {
                    $image_temp = image($file)->resize(['w' => 400]);
                }
                unset($image_temp->model);
                if ($image_temp) {
                    $return['files'][$file->pivot->file_key][] = $image_temp;
                }
            }
        }
        if ($categories->count() > 0) {
            $return['category'] = $this->buildArray($categories);
        }

        return $return;
    }

    private function getIdentity($identity, $data)
    {
        $return = [];
        if (array_key_exists('key', $data)) {
            $return[$data['key']] = $data['value'];
        } elseif ($identity === 'url') {
            $return['url'] = $data['url'];
        } else {
            if ($identity == 'details') $data = $data[0] ?? [];
            foreach ($data as $key => $value) {
                if (is_array($value)) {
                    $return += $this->getIdentity($key, $value);
                } else {
                    if (is_string($value)) {
                        $return[$identity . "_" . $key] = str_replace('http://{%url%}', config('api.static_url'), $value);
                    } else {
                        $return[$identity . "_" . $key] = $value;
                    }
                }
            }
        }
        return $return;
    }

}
