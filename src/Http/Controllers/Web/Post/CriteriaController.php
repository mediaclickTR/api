<?php

namespace Mediapress\API\Http\Controllers\Web\Post;

use Illuminate\Support\Facades\Validator;

use Mediapress\Modules\Content\AllBuilder\Creator\CreateRenderable;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\CategoryCriteria;
use Mediapress\Modules\Content\Models\Criteria;
use Mediapress\Modules\Content\Models\CriteriaDetail;
use Mediapress\Modules\Content\Models\CriteriaDetailExtra;
use Mediapress\Modules\Content\Models\CriteriaExtra;
use Mediapress\Modules\Content\Models\CriteriaType;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Content\Models\WebsiteCriteria;

use Mediapress\API\Http\Controllers\Web\PostApiController;

class CriteriaController extends PostApiController
{

    public function create()
    {
//        $hold = [
//            'category_id' => 3,
//            'criteria' => [
//                'sitemap_id' => 2,
//                'admin_id' => $this->user->id,
//                'status' => 1,
//            ],
//            'criteria_detail' => [
//                'name' => "Ürün Cinsi"
//            ],
//            'criteria_value' => [
//                "Tek Kişilik", "6 Kişilik", "12 Kişilik"
//            ]
//        ];
//        dd(json_encode($hold));
        $this->type = "CriteriaCreate";

        try {
            $this->checkAttributes();

            $this->status = true;
            $this->code = 200;
            $data = json_decode(request()->get('data'), 1);

            $validation = $this->validateCriteriaRequest($data);
            if (!is_null($validation)) {
                return $validation;
            }

            $category_id = $data['category_id'];
            $criteria_data = $data['criteria'];
            $criteria_detail_data = $data['criteria_detail'];
            $criteria_value_data = $data['criteria_value'];
            $criteria_detail_extra_data = $data['criteria_detail_extra'] ?? [];
            $criteria_extra_data = $data['criteria_extra'] ?? [];

            $sitemap = Sitemap::find($criteria_data['sitemap_id']);
            if (is_null($sitemap) || ($sitemap && $sitemap->criteria == 0)) {
                $this->payload = $this->emptyData("Sitemap does not have criteria attribute");
                return $this->setJson();
            }

            $category = Category::find($category_id);
            if (is_null($category) || ($sitemap && $sitemap->category == 0)) {
                $this->payload = $this->emptyData("Sitemap does not have category attribute");
                return $this->setJson();
            }

            //Criteria
            $criteria = Criteria::create(
                $criteria_data
            );


            //CriteriaDetail
            $detail = CriteriaDetail::firstOrCreate(
                [
                    'criteria_id' => $criteria->id,
                    'language_id' => $this->language->id,
                    'country_group_id' => $this->country_group ? $this->country_group->id : 1,
                ],
                $criteria_detail_data
            );

            //CategoryCriteria
            CategoryCriteria::firstOrCreate(
                [
                    'category_id' => $category_id,
                    'criteria_id' => $criteria->id,
                ]);

            //CriteriaValues
            foreach ($criteria_value_data as $value_data) {
                $value_criteria = Criteria::create(
                    [
                        'sitemap_id' => $criteria->sitemap_id,
                        'admin_id' => $this->user->id,
                        'status' => 1,
                        'criteria_id' => $criteria->id
                    ]
                );

                $slug = \Str::slug($value_data);
                $value_detail_criteria = CriteriaDetail::firstOrCreate(
                    [
                        'criteria_id' => $value_criteria->id,
                        'country_group_id' => $this->country_group->id,
                        'language_id' => $this->language->id,
                        'name' => $value_data,
                        'slug' => $slug,
                    ]
                );

            }


            //CriteriaDetailExtra
            if (count($criteria_detail_extra_data) > 0) {
                foreach ($criteria_detail_extra_data as $detail_extra_key => $detail_extra_value) {
                    CriteriaDetailExtra::firstOrCreate(
                        [
                            'criteria_detail_id' => $detail->id,
                            'key' => $detail_extra_key
                        ],
                        [
                            'value' => $detail_extra_value,
                        ]
                    );
                }
            }

            //CriteriaExtra
            if (count($criteria_extra_data) > 0) {
                foreach ($criteria_extra_data as $extra_key => $extra_value) {
                    CriteriaExtra::firstOrCreate(
                        [
                            'criteria_id' => $criteria->id,
                            'key' => $extra_key
                        ],
                        [
                            'value' => $extra_value,
                        ]
                    );
                }
            }

            $criteria = Criteria::where('id', $criteria->id)
                ->select('id')
                ->with(['extras', 'details' => function ($q) {
                    $q->where('language_id', $this->language->id)
                        ->with('extras', 'url');
                }])
                ->first();

            $this->payload = $this->buildArray($criteria);

        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }

        return $this->setJson();
    }

    public function edit($id)
    {
        $this->type = "CriteriaEdit";
        try {

            $this->checkAttributes();
            $data = json_decode(request()->get('data'), 1);


            $criteria_data = $data['criteria'] ?? [];
            $criteria_detail_data = $data['criteria_detail'] ?? [];
            $criteria_detail_extra_data = $data['criteria_detail_extra'] ?? [];
            $criteria_extra_data = $data['criteria_extra'] ?? [];


            $criteria = Criteria::where('id', $id)
                ->with(['extras', 'details' => function ($q) {
                    $q->where('language_id', $this->language->id)
                        ->with('extras', 'url');
                }])
                ->first();
            $detail = $criteria->details->first();
            if (is_null($criteria)) {
                $this->payload = $this->emptyData();
                return $this->setJson();
            }


            //Criteria
            if (count($criteria_data) > 0) {
                $criteria->update($criteria_data);
            }


            //CriteriaDetail
            if (count($criteria_detail_data) > 0) {

                if(!isset($criteria_detail_data['slug'])) {
                    $criteria_detail_data['slug'] = \Str::slug($criteria_detail_data['name']);
                }
                foreach ($criteria_detail_data as $detail_key => $detail_value) {
                    $detail->$detail_key = $detail_value;
                }
                $detail->save();

            }

            //CriteriaDetailExtra
            if (count($criteria_detail_extra_data) > 0) {
                foreach ($criteria_detail_extra_data as $detail_extra_key => $detail_extra_value) {
                    CriteriaDetailExtra::firstOrCreate(
                        [
                            'criteria_detail_id' => $detail->id,
                            'key' => $detail_extra_key
                        ],
                        [
                            'value' => $detail_extra_value,
                        ]
                    );
                }
            }

            //CriteriaExtra
            if (count($criteria_extra_data) > 0) {
                foreach ($criteria_extra_data as $extra_key => $extra_value) {
                    CriteriaExtra::firstOrCreate(
                        [
                            'criteria_id' => $criteria->id,
                            'key' => $extra_key
                        ],
                        [
                            'value' => $extra_value,
                        ]
                    );
                }
            }

            $this->payload = $this->buildArray($criteria);
        } catch
        (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }

        return $this->setJson();
    }

    public function delete($id)
    {
        $this->type = "CriteriaDelete";

        try {
            $criteria = Criteria::find($id);

            if (is_null($criteria)) {
                $this->payload = $this->emptyData();
                return $this->setJson();
            }


            //Criteria
            $criteria->delete();

            $this->payload = ['message' => $id . " id'li Criteria silinmiştir."];
        } catch
        (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();


    }


    private function validateCriteriaRequest($data)
    {
        $validator = Validator::make($data, [
            'category_id' => 'required',
            'criteria' => 'required|array',
            'criteria_detail' => 'required|array',
            'criteria_value' => 'required|array',
            'criteria_detail_extra' => 'filled|array',
            'criteria_extra' => 'filled|array',
        ]);

        if ($validator->fails()) {
            $this->payload = $this->emptyData($validator->errors()->all());
            return $this->setJson();
        }
    }


}


