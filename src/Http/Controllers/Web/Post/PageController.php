<?php

namespace Mediapress\API\Http\Controllers\Web\Post;

use Illuminate\Support\Facades\Validator;

use Mediapress\Modules\Content\AllBuilder\Creator\CreateRenderable;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\CategoryPage;
use Mediapress\Modules\Content\Models\CriteriaPage;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\PageDetailExtra;
use Mediapress\Modules\Content\Models\PageExtra;
use Mediapress\Modules\Content\Models\PageType;
use Mediapress\Modules\Content\Models\Website;

use Mediapress\API\Http\Controllers\Web\PostApiController;

class PageController extends PostApiController
{

    public function create()
    {
//        $hold = [
//            'category_id' => 30,
//            'page' => [
//                'sitemap_id' => 25,
//                'status' => 1,
//            ],
//            'page_detail' => [
//                'name' => "Test Page"
//            ],
//            'page_detail_extra' => [
//                'title' => "Test Title"
//            ],
//            'page_extra' => [
//                'date' => "Test Date"
//            ],
//            'page_criteria' => [
//                6
//            ]
//        ];
//        dd(json_encode($hold));
        $this->type = "PageCreate";

        try {

            $this->checkAttributes();
            $this->status = true;
            $this->code = 200;
            $data = json_decode(request()->get('data'), 1);

            $validation = $this->validatePageRequest($data);
            if (!is_null($validation)) {
                return $validation;
            }

            $page_data = $data['page'];
            $page_detail_data = $data['page_detail'];
            $page_detail_extra_data = $data['page_detail_extra'] ?? [];
            $page_extra_data = $data['page_extra'] ?? [];
            $page_criteria_data = $data['page_criteria'] ?? [];

            $sitemap = Sitemap::find($page_data['sitemap_id']);

            if (is_null($sitemap)) {
                $this->payload = $this->emptyData("Sitemap does not exist");
                return $this->setJson();
            }

            $sitemap_type = $sitemap->sitemapType;
            if($sitemap_type && $sitemap_type->sitemap_type_type != 'dynamic') {
                throw new \Exception("Sitemap type should be dynamic");
            }


            //Page
            $page = Page::preCreate(['sitemap_id' => $sitemap->id]);

            $page_data['admin_id'] = $this->user->id;
            $page_data['order'] = $page_data['order'] ?? 1;

            foreach ($page_data as $key => $value) {
                $page->$key = $value;
            }
            $page->save();
            $sitemap_details = $sitemap->details;

            foreach ($sitemap_details as $detail) {
                $detail_data = [
                    'page_id' => $page->id,
                    'language_id' => $detail->language_id,
                    'country_group_id' => $detail->country_group_id,
                ];
                PageDetail::create($detail_data);
            }


            //CategoryPage
            if (isset($data['category_id']) && $sitemap->categories->count() > 0) {

                CategoryPage::firstOrCreate(
                    [
                        'category_id' => $data['category_id'],
                        'page_id' => $page->id,
                    ]);
            }


            //PageDetail
            $page_detail_data += [
                'country_group_id' => $this->country_group ? $this->country_group->id : null,
                'website_id' => $this->website->id,
                'parent_id' => $page->id,
            ];
            $this->setUrl($page_detail_data, PageDetail::class);

            unset($page_detail_data['website_id'], $page_detail_data['parent_id']);

            $detail = PageDetail::firstOrCreate(
                [
                    'page_id' => $page->id,
                    'language_id' => $this->language->id
                ]
            );

            foreach ($page_detail_data as $detail_key => $detail_value) {
                $detail->$detail_key = $detail_value;
            }
            $detail->save();
            unset($detail->countryGroup, $detail->language, $detail->sitemap, $detail->parent, $detail->page, $detail->sitemap);


            //PageCriteria
            if (count($page_criteria_data) > 0) {
                $q = 'DELETE FROM criteria_page where page_id = ?';
                \DB::delete($q, [$page->id]);
                foreach ($page_criteria_data as $criteria_value) {
                    CriteriaPage::firstOrCreate(
                        [
                            'page_id' => $page->id,
                            'criteria_id' => $criteria_value
                        ]
                    );
                }
            }

            //PageDetailExtra
            if (count($page_detail_extra_data) > 0) {
                foreach ($page_detail_extra_data as $detail_extra_key => $detail_extra_value) {
                    PageDetailExtra::firstOrCreate(
                        [
                            'page_detail_id' => $detail->id,
                            'key' => $detail_extra_key
                        ],
                        [
                            'value' => $detail_extra_value,
                        ]
                    );
                }
            }

            //PageExtra
            if (count($page_extra_data) > 0) {
                foreach ($page_extra_data as $extra_key => $extra_value) {
                    PageExtra::firstOrCreate(
                        [
                            'page_id' => $page->id,
                            'key' => $extra_key
                        ],
                        [
                            'value' => $extra_value,
                        ]
                    );
                }
            }

            $this->payload = $this->buildArray($page);

        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }

        return $this->setJson();
    }

    public function edit($id)
    {
        $this->type = "PageEdit";

        try {

            $this->checkAttributes();
            $data = json_decode(request()->get('data'), 1);


            $page_data = $data['page'] ?? [];
            $page_detail_data = $data['page_detail'] ?? [];
            $page_detail_extra_data = $data['page_detail_extra'] ?? [];
            $page_extra_data = $data['page_extra'] ?? [];
            $page_criteria_data = $data['page_criteria'] ?? [];


            $page = Page::where('id', $id)
                ->with(['extras', 'details' => function ($q) {
                    $q->where('language_id', $this->language->id)
                        ->with('extras', 'url');
                }])
                ->first();
            $detail = $page->details->first();
            if (is_null($page)) {
                $this->payload = $this->emptyData();
                return $this->setJson();
            }


            //Page
            if (count($page_data) > 0) {
                $page->update($page_data);
            }


            //PageDetail
            if (count($page_detail_data) > 0) {

                $page_detail_data += [
                    'country_group_id' => $this->country_group ? $this->country_group->id : null,
                    'website_id' => $this->website->id,
                    'parent_id' => $page->id,
                ];
                $this->setUrl($page_detail_data, PageDetail::class);
                unset($page_detail_data['website_id'], $page_detail_data['parent_id']);

                $detail = $page->details->first();

                foreach ($page_detail_data as $detail_key => $detail_value) {
                    $detail->$detail_key = $detail_value;
                }
                $detail->save();

                unset($detail->countryGroup, $detail->language, $detail->page, $detail->parent);
            }



            //PageCriteria
            if (count($page_criteria_data) > 0) {
                $q = 'DELETE FROM criteria_page where page_id = ?';
                \DB::delete($q, [$page->id]);
                foreach ($page_criteria_data as $criteria_value) {
                    CriteriaPage::firstOrCreate(
                        [
                            'page_id' => $page->id,
                            'criteria_id' => $criteria_value
                        ]
                    );
                }
            }

            //PageDetailExtra
            if (count($page_detail_extra_data) > 0) {
                foreach ($page_detail_extra_data as $detail_extra_key => $detail_extra_value) {
                    PageDetailExtra::updateOrCreate(
                        [
                            'key' => $detail_extra_key,
                            'page_detail_id' => $detail->id
                        ],
                        [
                            'value' => $detail_extra_value,
                        ]
                    );
                }
            }

            //PageExtra
            if (count($page_extra_data) > 0) {
                foreach ($page_extra_data as $extra_key => $extra_value) {
                    PageExtra::updateOrCreate(
                        [
                            'key' => $extra_key,
                            'page_id' => $page->id
                        ],
                        [
                            'value' => $extra_value,
                        ]
                    );
                }
            }

            $this->payload = $this->buildArray($page);
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }

        return $this->setJson();
    }

    public function delete($id)
    {
        $this->type = "PageDelete";

        try {
            $page = Page::find($id);

            if (is_null($page)) {
                $this->payload = $this->emptyData();
                return $this->setJson();
            }


            //Page
            $page->delete();

            $this->payload = ['message' => $id . " id'li Page silinmiştir."];
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }

        return $this->setJson();

    }


    private function validatePageRequest($data)
    {
        $validator = Validator::make($data, [
            'page' => 'required|array',
            'page_detail' => 'required|array',
            'page_detail_extra' => 'filled|array',
            'page_extra' => 'filled|array',
            'category_id' => 'filled'
        ]);

        if ($validator->fails()) {
            $this->payload = $this->emptyData($validator->errors()->all());
            return $this->setJson();
        }
    }


}


