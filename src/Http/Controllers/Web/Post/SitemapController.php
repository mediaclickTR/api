<?php

namespace Mediapress\API\Http\Controllers\Web\Post;

use Illuminate\Support\Facades\Validator;

use Mediapress\Modules\Content\AllBuilder\Creator\CreateRenderable;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\Modules\Content\Models\SitemapDetailExtra;
use Mediapress\Modules\Content\Models\SitemapExtra;
use Mediapress\Modules\Content\Models\SitemapType;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Content\Models\WebsiteSitemap;
use Mediapress\Modules\MPCore\Facades\UserActionLog;

use Mediapress\API\Http\Controllers\Web\PostApiController;

class SitemapController extends PostApiController
{

    public function create()
    {
        $this->type = "SitemapCreate";

        try {

            $this->checkAttributes();

            $this->status = true;
            $this->code = 200;
            $data = json_decode(request()->get('data'), 1);

            $validation = $this->validateSitemapRequest($data);
            if (!is_null($validation)) {
                return $validation;
            }

            $sitemap_type_data = $data['sitemap_type'];
            $sitemap_data = $data['sitemap'];
            $sitemap_detail_data = $data['sitemap_detail'];
            $sitemap_detail_extra_data = $data['sitemap_detail_extra'] ?? [];
            $sitemap_extra_data = $data['sitemap_extra'] ?? [];

            //Sitemap Type
            $sitemap_type = SitemapType::firstOrCreate(
                [
                    'name' => $sitemap_type_data['name'],
                    'sitemap_type_type' => $sitemap_type_data['type']
                ]
            );

            //Sitemap
            $sitemap = Sitemap::preCreate(["feature_tag" => null]);
            if (!$sitemap->websites()->sync([$this->website->id])) {
                throw new \Exception("Sitemap Modeli Oluşturulamadı : " . json_encode($sitemap));
            }

            $sitemap_data['admin_id'] = $this->user->id;
            $sitemap_data['sitemap_type_id'] = $sitemap_type->id;

            foreach ($sitemap_data as $key => $value) {
                $sitemap->$key = $value;
            }
            $sitemap->save();


            //SitemapDetail
            $sitemap_detail_data += [
                'country_group_id' => $this->country_group ? $this->country_group->id : null,
                'website_id' => $this->website->id,
                'parent_id' => $sitemap->id,
            ];
            $this->setUrl($sitemap_detail_data, SitemapDetail::class);

            unset($sitemap_detail_data['website_id'], $sitemap_detail_data['parent_id']);

            $detail = SitemapDetail::firstOrCreate(
                [
                    'sitemap_id' => $sitemap->id,
                    'language_id' => $this->language->id
                ],
                $sitemap_detail_data
            );


            //SitemapDetailExtra
            if (count($sitemap_detail_extra_data) > 0) {
                foreach ($sitemap_detail_extra_data as $detail_extra_key => $detail_extra_value) {
                    SitemapDetailExtra::firstOrCreate(
                        [
                            'sitemap_detail_id' => $detail->id,
                            'key' => $detail_extra_key
                        ],
                        [
                            'value' => $detail_extra_value,
                        ]
                    );
                }
            }

            //SitemapExtra
            if (count($sitemap_extra_data) > 0) {
                foreach ($sitemap_extra_data as $extra_key => $extra_value) {
                    SitemapExtra::firstOrCreate(
                        [
                            'sitemap_id' => $sitemap->id,
                            'key' => $extra_key
                        ],
                        [
                            'value' => $extra_value,
                        ]
                    );
                }
            }

            $sitemap = Sitemap::with(['extras', 'details' => function ($q) {
                    $q->where('language_id', $this->language->id)
                        ->with('extras', 'url');
                }])
                ->find($sitemap->id);
            $this->createFiles($sitemap);
            unset($sitemap->sitemapType);

            $this->payload = $this->buildArray($sitemap);

        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }

        return $this->setJson();
    }

    public function edit($id)
    {

        $this->type = "SitemapEdit";
        try {

            $this->checkAttributes();
            $data = json_decode(request()->get('data'), 1);


            $sitemap_data = $data['sitemap'] ?? [];
            $sitemap_detail_data = $data['sitemap_detail'] ?? [];
            $sitemap_detail_extra_data = $data['sitemap_detail_extra'] ?? [];
            $sitemap_extra_data = $data['sitemap_extra'] ?? [];


            $sitemap = Sitemap::where('id', $id)
                ->with(['extras', 'details' => function ($q) {
                    $q->where('language_id', $this->language->id)
                        ->with('extras', 'url');
                }])
                ->first();

            if (is_null($sitemap)) {
                $this->payload = $this->emptyData("Sitemap does not exist!");
                return $this->setJson();
            }

            //Sitemap
            if (count($sitemap_data) > 0) {
                $sitemap->update($sitemap_data);
            }


            //SitemapDetail
            if (count($sitemap_detail_data) > 0) {

                $sitemap_detail_data += [
                    'country_group_id' => $this->country_group ? $this->country_group->id : null,
                    'website_id' => $this->website->id,
                    'parent_id' => $sitemap->id,
                ];
                $this->setUrl($sitemap_detail_data, SitemapDetail::class);

                unset($sitemap_detail_data['website_id'], $sitemap_detail_data['parent_id']);

                $detail = $sitemap->details->first();

                foreach ($sitemap_detail_data as $detail_key => $detail_value) {
                    $detail->$detail_key = $detail_value;
                }
                $detail->save();

                unset($detail->countryGroup, $detail->language, $detail->sitemap, $detail->parent);
            }

            //SitemapDetailExtra
            if (count($sitemap_detail_extra_data) > 0) {
                foreach ($sitemap_detail_extra_data as $detail_extra_key => $detail_extra_value) {
                    SitemapDetailExtra::updateOrCreate(
                        [
                            'key' => $detail_extra_key,
                            'sitemap_detail_id' => $sitemap->details->first()->id
                        ],
                        [
                            'value' => $detail_extra_value,
                        ]
                    );
                }
            }

            //SitemapExtra
            if (count($sitemap_extra_data) > 0) {
                foreach ($sitemap_extra_data as $extra_key => $extra_value) {
                    SitemapExtra::updateOrCreate(
                        [
                            'key' => $extra_key,
                            'sitemap_id' => $sitemap->id
                        ],
                        [
                            'value' => $extra_value,
                        ]
                    );
                }
            }


            $this->payload = $this->buildArray($sitemap);

        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }

        return $this->setJson();
    }

    public function delete($id)
    {
        $this->type = "SitemapDelete";

        try {
            $sitemap = Sitemap::find($id);

            if (is_null($sitemap)) {
                $this->payload = $this->emptyData();
                return $this->setJson();
            }


            //Sitemap
            $sitemap->delete();

            $this->payload = ['message' => $id . " id'li Sitemap silinmiştir."];
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();


    }


    private function validateSitemapRequest($data)
    {
        $validator = Validator::make($data, [
            'sitemap_type' => 'required|array',
            'sitemap' => 'required|array',
            'sitemap_detail' => 'required|array',
            'sitemap_detail_extra' => 'filled|array',
            'sitemap_extra' => 'filled|array',
        ]);

        if ($validator->fails()) {
            $this->payload = $this->emptyData($validator->errors()->all());
            return $this->setJson();
        }
    }



    private function createRenderables($sitemap)
    {
        /** @var Sitemap $sitemap */
        $renderable = new CreateRenderable($sitemap);
        $do = $renderable->createSitemapRenderable()
            && ($sitemap->sitemapType->sitemap_type_type != "static" && $renderable->createPagesRenderable())
            && ($sitemap->category && $renderable->createCategoriesRenderable())
            && ($sitemap->property && $renderable->createPropertiesRenderable())
            && ($sitemap->criteria && $renderable->createCriteriasRenderable());
        unset($files);
    }
    private function createController($sitemap)
    {
        $name = $sitemap->sitemapType->name;
        $Name = ucfirst($name);

        $folder = app_path(implode(DIRECTORY_SEPARATOR, ["Modules", "Content", "Http", "Controllers", "Web"]));


        $file2create_path = $folder . DIRECTORY_SEPARATOR . $Name . "Controller.php";

        if (is_file($file2create_path)) {
            return true;
        }

        $class_namespace = "App\Modules\Content\Http\Controllers\Web";
        $class_name = $Name . "Controller";

        $data = file_get_contents(base_path('vendor/mediapress/mediapress/src/Modules/Content/AllBuilder/Creator/Controller.template'));

        $replace_keys = ['{%namespace%}', '{%class%}'];
        $replace_values = [$class_namespace, $class_name];

        $replace_keys[] = '{%SitemapDetailFunction%}';
        $replace_values[] = $sitemap->detailPage || $sitemap->sitemapType->sitemap_type_type == "static" ? "public function SitemapDetail(){\n\n\n\t}" : "";

        $replace_keys[] = '{%CategoryDetailFunction%}';
        $replace_values[] = $sitemap->category ? "public function CategoryDetail(){\n\n\n\t}" : "";

        $replace_keys[] = '{%PageDetailFunction%}';
        $replace_values[] = $sitemap->sitemapType->sitemap_type_type != "static" ? "public function PageDetail(){\n\n\n\t}" : "";

        $replaced_data = str_replace($replace_keys, $replace_values, $data);

        if (!file_exists($folder)) {

            $oldmask = umask(0);
            mkdir($folder, 0777, true);
            umask($oldmask);
        }
        file_put_contents($file2create_path, $replaced_data);

        unset($name, $Name, $folder, $file2create_path, $class_name, $class_namespace, $data, $replace_keys, $replace_values, $replaced_data);

        return true;

    }
    private function createFiles($sitemap)
    {

        $this->createRenderables($sitemap);
        $this->createController($sitemap);

    }
}


