<?php

namespace Mediapress\API\Http\Controllers\Web\Post;

use Illuminate\Support\Facades\Validator;

use Mediapress\Modules\Content\AllBuilder\Creator\CreateRenderable;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\CategoryDetail;
use Mediapress\Modules\Content\Models\CategoryDetailExtra;
use Mediapress\Modules\Content\Models\CategoryExtra;
use Mediapress\Modules\Content\Models\CategoryType;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Content\Models\WebsiteCategory;

use Mediapress\API\Http\Controllers\Web\PostApiController;

class CategoryController extends PostApiController
{

    public function create()
    {
//        $hold = [
//            'category' => [
//                'sitemap_id' => 2,
//                'status' => 1,
//                'category_id' => 7
//            ],
//            'category_detail' => [
//                'name' => "Tepsi"
//            ]
//        ];
//        dd(json_encode($hold));
        $this->type = "CategoryCreate";

        try {

            $this->checkAttributes();

            $this->status = true;
            $this->code = 200;
            $data = json_decode(request()->get('data'), 1);

            $validation = $this->validateCategoryRequest($data);
            if (!is_null($validation)) {
                return $validation;
            }

            $category_data = $data['category'];
            $category_detail_data = $data['category_detail'];
            $category_detail_extra_data = $data['category_detail_extra'] ?? [];
            $category_extra_data = $data['category_extra'] ?? [];

            $sitemap = Sitemap::find($category_data['sitemap_id']);

            if (is_null($sitemap) || ($sitemap && $sitemap->category == 0)) {
                $this->payload = $this->emptyData("Sitemap does not have category attribute");
                return $this->setJson();
            }

            //Category
            $category_data['admin_id'] = $this->user->id;
            $category = Category::create(
                $category_data
            );



            //CategoryDetail
            $category_detail_data += [
                'country_group_id' => $this->country_group ? $this->country_group->id : 1,
                'website_id' => $this->website->id,
                'parent_id' => $category->id,
            ];
            $this->setUrl($category_detail_data, CategoryDetail::class);
            unset($category_detail_data['website_id'], $category_detail_data['parent_id']);


            $detail = CategoryDetail::firstOrCreate(
                [
                    'category_id' => $category->id,
                    'language_id' => $this->language->id
                ]
            );
            foreach ($category_detail_data as $detail_key => $detail_value) {
                $detail->$detail_key = $detail_value;
            }
            $detail->save();


            //CategoryDetailExtra
            if (count($category_detail_extra_data) > 0) {
                foreach ($category_detail_extra_data as $detail_extra_key => $detail_extra_value) {
                    CategoryDetailExtra::firstOrCreate(
                        [
                            'category_detail_id' => $detail->id,
                            'key' => $detail_extra_key
                        ],
                        [
                            'value' => $detail_extra_value,
                        ]
                    );
                }
            }

            //CategoryExtra
            if (count($category_extra_data) > 0) {
                foreach ($category_extra_data as $extra_key => $extra_value) {
                    CategoryExtra::firstOrCreate(
                        [
                            'category_id' => $category->id,
                            'key' => $extra_key
                        ],
                        [
                            'value' => $extra_value,
                        ]
                    );
                }
            }

            $category = Category::where('id', $category->id)
                ->select('id')
                ->with(['extras', 'details' => function ($q) {
                    $q->where('language_id', $this->language->id)
                        ->with('extras', 'url');
                }])
                ->first();

            $this->payload = $this->buildArray($category);

        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }

        return $this->setJson();
    }

    public function edit($id)
    {
        $this->type = "CategoryEdit";
        try {

            $this->checkAttributes();
            $data = json_decode(request()->get('data'), 1);


            $category_data = $data['category'] ?? [];
            $category_detail_data = $data['category_detail'] ?? [];
            $category_detail_extra_data = $data['category_detail_extra'] ?? [];
            $category_extra_data = $data['category_extra'] ?? [];


            $category = Category::where('id', $id)
                ->with(['extras', 'details' => function ($q) {
                    $q->where('language_id', $this->language->id)
                        ->with('extras', 'url');
                }])
                ->first();
            $detail = $category->details->first();
            if (is_null($category)) {
                $this->payload = $this->emptyData();
                return $this->setJson();
            }


            //Category
            if (count($category_data) > 0) {
                $category->update($category_data);
            }


            //CategoryDetail
            if (count($category_detail_data) > 0) {

                $category_detail_data += [
                    'country_group_id' => $this->country_group ? $this->country_group->id : null,
                    'website_id' => $this->website->id,
                    'parent_id' => $category->id,
                ];
                if(isset($category_detail_data['name'])) {
                    $this->setUrl($category_detail_data, CategoryDetail::class);
                }
                unset($category_detail_data['website_id'], $category_detail_data['parent_id']);

                $detail = $category->details->first();

                foreach ($category_detail_data as $detail_key => $detail_value) {
                    $detail->$detail_key = $detail_value;
                }
                $detail->save();

                unset($detail->countryGroup, $detail->language, $detail->sitemap, $detail->parent);
            }

            //CategoryDetailExtra
            if (count($category_detail_extra_data) > 0) {
                foreach ($category_detail_extra_data as $detail_extra_key => $detail_extra_value) {
                    CategoryDetailExtra::firstOrCreate(
                        [
                            'category_detail_id' => $detail->id,
                            'key' => $detail_extra_key
                        ],
                        [
                            'value' => $detail_extra_value,
                        ]
                    );
                }
            }

            //CategoryExtra
            if (count($category_extra_data) > 0) {
                foreach ($category_extra_data as $extra_key => $extra_value) {
                    CategoryExtra::firstOrCreate(
                        [
                            'category_id' => $category->id,
                            'key' => $extra_key
                        ],
                        [
                            'value' => $extra_value,
                        ]
                    );
                }
            }

            $this->payload = $this->buildArray($category);
        } catch
        (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }

        return $this->setJson();
    }

    public function delete($id)
    {
        $this->type = "CategoryDelete";

        try {
            $category = Category::find($id);

            if (is_null($category)) {
                $this->payload = $this->emptyData();
                return $this->setJson();
            }


            //Category
            $category->delete();

            $this->payload = ['message' => $id . " id'li Category silinmiştir."];
        } catch
        (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();


    }


    private function validateCategoryRequest($data)
    {
        $validator = Validator::make($data, [
            'category' => 'required|array',
            'category_detail' => 'required|array',
            'category_detail_extra' => 'filled|array',
            'category_extra' => 'filled|array',
        ]);

        if ($validator->fails()) {
            $this->payload = $this->emptyData($validator->errors()->all());
            return $this->setJson();
        }
    }


}


