<?php

namespace Mediapress\API\Http\Controllers\Web\Post;

use Mediapress\Traits\HasMFiles;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\CategoryDetail;
use Mediapress\FileManager\Models\MFileGeneral;
use Mediapress\Modules\MPCore\Facades\FilesEngine;

use Mediapress\API\Http\Controllers\Web\PostApiController;

class FileController extends PostApiController
{
    use HasMFiles;

    private $classes = [
        'page' => Page::class,
        'page_detail' => PageDetail::class,
        'sitemap' => Sitemap::class,
        'sitemap_detail' => SitemapDetail::class,
        'category' => Category::class,
        'category_detail' => CategoryDetail::class,
    ];

    public function index()
    {
        $this->type = 'imageCreate';
        try {
            $this->status = true;
            $this->code = 200;
            $this->payload = $this->getPayload(null);
        } catch (\Exception $exception) {
            $this->status = false;
            $this->code = 503;
            $this->payload = $this->getError($exception);
        }
        return $this->setJson();
    }

    function getPayload($id)
    {
        $type = request()->get('type');
        $id = request()->get('id');
        $file = request()->file('file');
        $disk_key = request()->get('disk_key');
        $file_key = request()->get('file_key');


        if(is_null($file_key) || is_null($type) || !isset($this->classes[$type]) || is_null($id) || is_null($file) || is_null($disk_key)) {
            return $this->emptyData();
        }

        $process_id = uniqid();
        $folder_path = date('Y/m');
        $save = FilesEngine::easySaveUploadedFile($disk_key, $file, $folder_path, null, $process_id);


        $model = $this->classes[$type];
        $mfile = $save['data']['MFile'];

        $mfile_general = MFileGeneral::firstOrCreate(
            [
                'mfile_id' => $mfile->id,
                'model_type' => $model,
                'model_id' => $id,
                'file_key' => $file_key

            ]
        );

        return $mfile_general->toArray();

    }


}


