<?php

namespace Mediapress\API\Http\Controllers\Web;


use Mediapress\Modules\Content\Models\Meta;
use \Mediapress\Modules\MPCore\Models\Language;
use \Mediapress\Modules\MPCore\Models\CountryGroup;
use \Mediapress\Modules\Content\Models\LanguageWebsite;
use Mediapress\Modules\MPCore\Models\Url;
use Mediapress\Modules\Content\Models\Website;

class ApiController
{
    protected $status = true;
    protected $payload;
    protected $language;
    protected $type = "";
    protected $code = 200;

    protected $category_status;
    protected $sitemap_status;
    protected $page_status;
    protected $pagination_status;
    protected $gallery_status;
    protected $slider_status;
    protected $criteria_status;

    protected $page_category_status;

    protected $per_page = 10;

    public function __construct()
    {
        $this->language = $this->setCurrentLanguage();

        $this->sitemap_status = $this->setSitemapStatus();
        $this->category_status = $this->setCategoryStatus();
        $this->page_status = $this->setPageStatus();
        $this->pagination_status = $this->setPaginationStatus();
        $this->gallery_status = $this->setGalleryStatus();
        $this->slider_status = $this->setSliderStatus();
        $this->criteria_status = $this->setCriteriaStatus();

        $this->page_category_status = $this->setPageCategoryStatus();

        $this->per_page = $this->setPerPage();

        if (app()->getLocale() != $this->language['code']) {
            app()->setLocale($this->language['code']);
        }
    }

    protected function getPayload($id)
    {
        return array();
    }

    protected function getError(\Exception $exception)
    {
        return [
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
        ];
    }

    protected function emptyData()
    {
        $this->code = 404;
        return [
            'message' => "Data Not Found"
        ];
    }

    protected function setJson()
    {
        $temp = [
            'success' => $this->status,
            'code' => $this->code,
            'response' => [
                'current_language' => $this->language,
                'type' => $this->type,
                'data' => $this->payload
            ]
        ];
        return response()->json($temp, $this->code)->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
            ->header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, X-Token-Auth, Authorization');
    }

    protected function getStats()
    {
        $hold = array();
        if ($this->type == 'pages') {
            $hold['category_status'] = $this->category_status;
            $hold['criteria_status'] = $this->criteria_status;
        }
        if ($this->type == 'sitemap') {
            $hold['category_status'] = $this->category_status;
            $hold['pages_status'] = $this->page_status;
            $hold['page_category_status'] = $this->page_category_status;
            $hold['pagination_status'] = $this->pagination_status;
            $hold['criteria_status'] = $this->criteria_status;
        }
        if ($this->type == 'category') {
            $hold['sitemap_status'] = $this->sitemap_status;
            $hold['pages_status'] = $this->page_status;
            $hold['pagination_status'] = $this->pagination_status;
            $hold['criteria_status'] = $this->criteria_status;
        }

        if ($this->pagination_status)
            $hold['perPage'] = (int)$this->per_page;

        return $hold;
    }

    protected function cacheKey(array $data = array())
    {
        $request = request();
        $url = $request->getRequestUri();

        $key = $url . implode(".", $data) . "MEDIAPRESS" . "API" . "v4";
        return hash('md5', $key);
    }

    protected function getDefaultLanguage()
    {
        if (app()->runningInConsole()) {
            $default_language = CountryGroup::where("code", "gl")->first()
                ->languages()->where('country_group_language.default', 1)
                ->first();
        } else {
            $domain = request()->server('HTTP_HOST');
            $region = Website::where('slug', $domain)->first()->regions;
            $language = Language::where('code', $this->browser_languages())->first();

            if ($region == 0) {
                $country_group = CountryGroup::where("code", "gl")->whereHas('languages', function ($q) use ($language) {
                    $q->where("id", $language->id);
                })->first();
            } else {
                $ipDetails = $this->ip_details();
                if ($ipDetails) {
                    $ipDetails = $ipDetails["country"]["iso_code"];
                }
                $country_group = CountryGroup::whereHas("countries", function ($q) use ($ipDetails) {
                    $q->where("code", $ipDetails);
                })->whereHas('languages', function ($q) use ($language) {
                    $q->where("id", $language->id);
                })->first();
            }

            if ($country_group) {
                $default_language = $country_group->languages()->where('country_group_language.default', 1)->first();
            } else {
                $default_language = CountryGroup::where("code", "gl")->first()
                    ->languages()->where('country_group_language.default', 1)
                    ->first();
            }
        }

        return [
            'id' => $default_language->id,
            'code' => $default_language->code,
            'name' => $default_language->name,
            'regional' => $default_language->regional,
            'alphabet' => $default_language->alphabet,
        ];
    }

    protected function setCurrentLanguage()
    {
        if (request()->get('language')) {
            $language_code = request()->get('language');
            $language_id = Language::where('code', strtolower($language_code))->select('id', 'code', 'name', 'regional')->first()->id;
        } else {
            $default_language = $this->getDefaultLanguage();
            $language_id = $default_language['id'];
            $language_code = $default_language['code'];
        }

        return [
            'id' => $language_id,
            'code' => $language_code,
        ];
    }

    protected function getMeta($url)
    {
        $url = Url::where('url', $url)->first();
        if ($url) {
            return $url->metaWorker->setUrlBase(config('mediapress.vue_url'))->toVueClass();
        }
    }

    protected function browser_languages()
    {
        return strtolower(substr(request()->server('HTTP_ACCEPT_LANGUAGE'), 0, 2));
    }

    protected function ip_details($ip = null)
    {
        if (!$ip) {
            if (request()->server('HTTP_CF_CONNECTING_IP')) {
                $ip = request()->server('HTTP_CF_CONNECTING_IP');
            } else {
                $ip = request()->server('REMOTE_ADDR');
            }
        }
        return json_decode(file_get_contents('https://geoipmediaclick.appspot.com/?ip=' . $ip), true);
    }

    protected function buildSearchArray($data)
    {
        if (is_a($data, "Illuminate\Database\Eloquent\Collection")) {
            $return = [];
            foreach ($data as $key => $page) {
                $return[] = $this->buildArray($page);
            }
            return $return;
        } else {
            return $this->getSingleArray($data);
        }
    }

    protected function buildArray($data)
    {
        if (is_a($data, "Illuminate\Database\Eloquent\Collection")) {
            $return = [];
            foreach ($data as $key => $page) {
                $return[] = $this->buildArray($page);
            }
            return $return;
        } else {
            return $this->getSingleArray($data);
        }
    }

    private function getSingleArray($page)
    {
        $temp = $page->toArray();
        $categories = collect();
        if (isset($temp['categories'])) {
            unset($temp['categories']);
            $categories = $page->categories;
        }
        $return = [];
        foreach ($temp as $key => $value) {
            if (is_array($value)) {
                $return += $this->getIdentity($key, $value);
            } else {
                $return[$key] = $value;
            }
        }
        if ($page->details && $page->details->first()) {
            $return['metas'] = $this->getMeta($page->details->first()->url->url);
        }
        if ($page->mfiles) {
            foreach ($page->mfiles as $file) {
                if (request()->get('w')) {
                    $image_temp = image($file)->resize(['w' => request()->get('w')]);
                } else {
                    $image_temp = image($file)->resize(['w' => 400]);
                }
                unset($image_temp->model);
                if ($image_temp) {
                    $return['files'][$file->pivot->file_key][] = $image_temp;
                }
            }
        }
        if ($categories->count() > 0) {
            $return['category'] = $this->buildArray($categories);
        }

        return $return;
    }

    private function getIdentity($identity, $data)
    {
        $return = [];
        if (array_key_exists('key', $data)) {
            $return[$data['key']] = $data['value'];
        } elseif ($identity === 'url') {
            $return['url'] = $data['url'];
        } else {
            if ($identity == 'details') {
                $data = $data[0] ?? [];
            }
            foreach ($data as $key => $value) {
                if (is_array($value)) {
                    $return += $this->getIdentity($key, $value);
                } else {
                    if (is_string($value)) {
                        $return[$identity . "_" . $key] = str_replace('http://{%url%}', config('api.static_url'), $value);
                    } else {
                        $return[$identity . "_" . $key] = $value;
                    }
                }
            }
        }
        return $return;
    }

    protected function getOrder()
    {
        $hold = explode(';', request()->get('order'));
        if (!request()->get('order')) {
            return ['order' => 'asc'];
        }
        $return = array();
        foreach ($hold as $h) {
            if ($h == "") {
                continue;
            }

            $temp = explode(',', $h);

            $return[$temp[0]] = $temp[1] ?? "asc";
        }
        return $return;
    }

    protected function getQuery($type)
    {
        $var = request()->get($type);

        $return = array();
        if (!is_null($var)) {
            $hold = explode(';', $var);
            foreach ($hold as $h) {
                if ($h == "") {
                    continue;
                }

                $temp = explode(',', $h);

                if (!isset($temp[0])) {
                    continue;
                }

                if ($temp[0] == 'where') {
                    if (count($temp) >= 2) {
                        $return[] = $temp;
                    } else {
                        continue;
                    }
                } elseif ($temp[0] == 'whereIn' || $temp[0] == 'whereNotIn') {
                    $temp = explode(',', $h, 3);
                    $temp[2] = json_decode($temp[2]);
                    $return[] = $temp;
                }
            }
        }
        return $return;
    }

    protected function setCategoryStatus()
    {
        if (request()->get('category') == 1)
            return true;
        return false;
    }

    protected function setSitemapStatus()
    {
        if (request()->get('sitemap') == 1)
            return true;
        return false;
    }

    protected function setPageStatus()
    {
        if (request()->get('pages') == 1)
            return true;
        return false;
    }

    protected function setPaginationStatus()
    {
        if (request()->get('pagination') == 1)
            return true;
        return false;
    }

    protected function setGalleryStatus()
    {
        if (request()->get('gallery') == 1)
            return true;
        return false;
    }

    protected function setSliderStatus()
    {
        if (request()->get('slider') == 1)
            return true;
        return false;
    }

    protected function setCriteriaStatus()
    {
        if (request()->get('criteria') == 1)
            return true;
        return false;
    }

    protected function setPageCategoryStatus()
    {
        if (request()->get('pageCategory') == 1)
            return true;
        return false;
    }

    protected function setPerPage()
    {
        if (request()->get('perPage'))
            return request()->get('perPage');
        return 10;
    }


}
