<?php

Route::get('/menu', "Get\MenuController@index");
Route::get('/base', "Get\BaseController@index");
Route::get('/socialMedia/{group_id}', "Get\SocialMediaController@index");
Route::get('/slider/{id?}', "Get\SliderController@index");
Route::get('/form/{id?}', "Get\FormController@index");
Route::get('/message/{id}', "Get\MessageController@index");
Route::get('/category/{id?}', "Get\CategoryController@index");
Route::get('/sitemap/{id}', "Get\SitemapController@index");
Route::get('/page/{id?}', "Get\PageController@index");
Route::get('/search/{key}', "Get\SearchController@index");
Route::get('/url', "Get\UrlController@index");
Route::get('/ebulletin/{code}', "Get\EbulletinController@index");
Route::post('/ebulletin', "Get\EbulletinController@index");


Route::prefix('Sitemap')->group(function() {
    Route::post('create', 'Post\SitemapController@create');
    Route::put('edit/{id}', 'Post\SitemapController@edit');
    Route::delete('delete/{id}', 'Post\SitemapController@delete');
});

Route::prefix('Category')->group(function() {
    Route::post('create', 'Post\CategoryController@create');
    Route::put('edit/{id}', 'Post\CategoryController@edit');
    Route::delete('delete/{id}', 'Post\CategoryController@delete');
});

Route::prefix('Criteria')->group(function() {
    Route::post('create', 'Post\CriteriaController@create');
    Route::put('edit/{id}', 'Post\CriteriaController@edit');
    Route::delete('delete/{id}', 'Post\CriteriaController@delete');
});

Route::prefix('Page')->group(function() {
    Route::post('create', 'Post\PageController@create');
    Route::put('edit/{id}', 'Post\PageController@edit');
    Route::delete('delete/{id}', 'Post\PageController@delete');
});


Route::get('diskkey', "Get\DiskKeyController@index");
Route::post('File/create', "Post\FileController@index");
