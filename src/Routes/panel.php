<?php



Route::group(['prefix' => 'api', 'middleware' => 'panel.auth'], function () {
    Route::get('/docs', 'ApiController@index');
    Route::get('/json', 'ApiController@json');
});
